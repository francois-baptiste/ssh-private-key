from setuptools import setup

setup(name='helloworld',
      version='0.1',
      description='hello world pip package library',
      url='https://gitlab.com/francois-baptiste/ssh-private-key.git',
      author='Francois Baptiste',
      author_email='hello@world.com',
      license='none',
      packages=['helloworld']
      )
